from rest_framework_json_api import serializers as spa_serializers

from core.models import UserCoins, Drink, MachineCoins


# Сериализатор для монет клиента.
class UserCoinsSerializer(spa_serializers.HyperlinkedModelSerializer):

	class Meta:
		model = UserCoins
		fields = ('coin_type', 'amount', )


# Сериализатор для напитков.
class DrinkSerializer(spa_serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Drink
		fields = ('drink_type', 'amount', 'price', )


# Сериализатор модели монет кофемашины.
class MachineCoinsSerializer(spa_serializers.HyperlinkedModelSerializer):

	class Meta:
		model = MachineCoins
		fields = ('coin_type', 'amount', )