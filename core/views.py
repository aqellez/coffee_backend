from rest_framework import viewsets, permissions, authentication
from django_filters.rest_framework import DjangoFilterBackend

from core.models import UserCoins, Drink, MachineCoins
from core.serializers import UserCoinsSerializer, DrinkSerializer, MachineCoinsSerializer


# Монеты клиента.
class UserCoinsViewSet(viewsets.ModelViewSet):
	permission_classes = (permissions.AllowAny, )
	allowed_methods = ['GET', 'PUT']
	queryset = UserCoins.objects.all()
	serializer_class = UserCoinsSerializer

	filter_backends = (DjangoFilterBackend,)
	filter_fields = ('coin_type', 'amount', )


# Виды напитков.
class DrinkViewSet(viewsets.ModelViewSet):
	permission_classes = (permissions.AllowAny, )
	allowed_methods = ['GET']
	queryset = Drink.objects.all()
	serializer_class = DrinkSerializer

	filter_backends = (DjangoFilterBackend,)
	filter_fields = ('drink_type', )


# Монеты в кофемашине.
class MachineCoinsViewSet(viewsets.ModelViewSet):
	permission_classes = (permissions.AllowAny, )
	allowed_methods = ['GET', 'PUT']
	queryset = MachineCoins.objects.all()
	serializer_class = MachineCoinsSerializer

	filter_backends = (DjangoFilterBackend,)
	filter_fields = ('coin_type', 'amount', )