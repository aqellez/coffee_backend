from django.db import models

# Create your models here.


class UserCoins(models.Model):
	id = models.AutoField(primary_key=True)
	coin_type = models.CharField(max_length=20)
	amount = models.IntegerField()


class Drink(models.Model):
	id = models.AutoField(primary_key=True)
	drink_type = models.CharField(max_length=20)
	amount = models.IntegerField()
	price = models.IntegerField()


class MachineCoins(models.Model):
	id = models.AutoField(primary_key=True)
	coin_type = models.CharField(max_length=10)
	amount = models.IntegerField()