from django.conf.urls import url, include
from rest_framework import routers

from core.views import UserCoinsViewSet, DrinkViewSet, MachineCoinsViewSet


router = routers.DefaultRouter()
router.register(r'user_coins', UserCoinsViewSet)
router.register(r'drink', DrinkViewSet)
router.register(r'machine_coins', MachineCoinsViewSet)


urlpatterns = [
	url(r'^api/', include(router.urls)),
]